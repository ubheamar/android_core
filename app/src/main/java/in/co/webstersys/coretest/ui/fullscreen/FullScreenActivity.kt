package `in`.co.webstersys.coretest.ui.fullscreen

import `in`.co.webstersys.core.ui.base.BaseFullScreenActivity
import `in`.co.webstersys.core.utils.ImmersiveModeListener
import `in`.co.webstersys.coretest.R
import `in`.co.webstersys.coretest.databinding.ActivityFullScreenBinding
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil.setContentView
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.transition.Visibility
import android.view.View


class FullScreenActivity : BaseFullScreenActivity<ActivityFullScreenBinding>(),ImmersiveModeListener {
    var visibility: Int = View.GONE
    companion object {
        fun startActivity(context: Context) {
            val intent = Intent(context, FullScreenActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onChangeImmersiveMode(show: Boolean) {
        showToast("Toggle:"+show)
    }

    override val immersiveModeListener: ImmersiveModeListener?
        get() = this
    override val layoutResource: Int
        get() = R.layout.activity_full_screen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding.btnToggle.setOnClickListener {
            toggle(visibility)
            if (visibility==View.VISIBLE)
                visibility = View.GONE
            else
                visibility = View.VISIBLE

        }
    }
}
