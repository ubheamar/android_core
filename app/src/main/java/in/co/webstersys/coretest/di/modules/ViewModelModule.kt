package `in`.co.webstersys.coretest.di.modules


import `in`.co.webstersys.coretest.ui.main.MainViewModel
import `in`.co.webstersys.core.utils.AppViewModelFactory
import `in`.co.webstersys.core.utils.ViewModelKey
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindUserViewModel(mainViewModel: MainViewModel): ViewModel
    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory
}