package `in`.co.webstersys.coretest.di.modules

import `in`.co.webstersys.coretest.di.modules.FragmentBuildersModule
import `in`.co.webstersys.coretest.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity
}
