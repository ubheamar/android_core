package `in`.co.webstersys.coretest.di.modules

import dagger.Module

@Module(includes = [ViewModelModule::class])
class AppModule {
}