package `in`.co.webstersys.coretest.ui.nav

import `in`.co.webstersys.core.ui.base.BaseNavActivity
import `in`.co.webstersys.coretest.R
import `in`.co.webstersys.coretest.databinding.ActivityNavBinding
import `in`.co.webstersys.coretest.ui.fullscreen.FullScreenActivity
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil.setContentView
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.mikepenz.ionicons_typeface_library.Ionicons
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem

class NavActivity : BaseNavActivity<ActivityNavBinding>() {

    internal var trending = PrimaryDrawerItem().withIdentifier(1).withName("Trending").withIcon(Ionicons.Icon.ion_arrow_graph_up_right)
    internal var hollywood = PrimaryDrawerItem().withIdentifier(2).withName("Hollywood").withIcon(Ionicons.Icon.ion_ios_film)
    internal var bollywood = PrimaryDrawerItem().withIdentifier(3).withName("Hollywood").withIcon(Ionicons.Icon.ion_social_bitcoin)
    internal var tv_shows = PrimaryDrawerItem().withIdentifier(4).withName("TV shows").withIcon(Ionicons.Icon.ion_android_desktop)


    override fun onDrawerItemClick(view: View, identifier: Int, iDrawerItem: IDrawerItem<*, *>) {
       showToast("Item Clicked Id:"+identifier);
    }

    override val layoutResource: Int
        get() = R.layout.activity_nav

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addMenuItems()
    }

    private fun addMenuItems() {
        if(drawer!=null){
            drawer?.addItems(trending, hollywood, bollywood, DividerDrawerItem(), tv_shows)
        }
    }
    companion object {
        fun startActivity(context: Context) {
            val intent = Intent(context, NavActivity::class.java)
            context.startActivity(intent)
        }
    }

}
