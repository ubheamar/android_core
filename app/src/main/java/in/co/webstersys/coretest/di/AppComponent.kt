package `in`.co.webstersys.coretest.di

import `in`.co.webstersys.coretest.app.App
import `in`.co.webstersys.coretest.di.modules.AppModule
import `in`.co.webstersys.coretest.di.modules.FullScreenActivityModule
import `in`.co.webstersys.coretest.di.modules.MainActivityModule
import `in`.co.webstersys.coretest.di.modules.NavActivityModule
import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidInjectionModule::class,
            AppModule::class,
            MainActivityModule::class,
            FullScreenActivityModule::class,
            NavActivityModule::class
        ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}