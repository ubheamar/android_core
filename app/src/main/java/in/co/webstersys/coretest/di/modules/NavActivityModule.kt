package `in`.co.webstersys.coretest.di.modules


import `in`.co.webstersys.coretest.ui.nav.NavActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class NavActivityModule {
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeNavActivityModule(): NavActivity
}
