package `in`.co.webstersys.coretest.ui.main

import `in`.co.webstersys.core.ui.base.BaseActivity
import `in`.co.webstersys.coretest.R
import `in`.co.webstersys.coretest.databinding.ActivityMainBinding
import `in`.co.webstersys.coretest.ui.fullscreen.FullScreenActivity
import `in`.co.webstersys.coretest.ui.nav.NavActivity
import android.os.Bundle

class MainActivity : BaseActivity<ActivityMainBinding>(){
    override val layoutResource: Int
        get() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding.btnFullScreen.setOnClickListener {
            FullScreenActivity.startActivity(this)
        }
        dataBinding.btnNavDrawer.setOnClickListener{
            NavActivity.startActivity(this)
        }
    }
}
