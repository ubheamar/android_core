package `in`.co.webstersys.core.ui.base

import `in`.co.webstersys.core.R
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.Toast

import com.mikepenz.iconics.context.IconicsContextWrapper
import dagger.android.AndroidInjection
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import javax.inject.Inject


/**
 * Created by ua989 on 2/22/2018.
 */
abstract class BaseActivity<DB : ViewDataBinding> : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var dataBinding: DB
    protected var toolbar: Toolbar? = null

    protected abstract val layoutResource: Int
    protected override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, layoutResource)
        setupToolbar()
    }

    protected fun setupToolbar() {
        toolbar =  findViewById<Toolbar>(R.id.toolbar)
        if(toolbar!=null)
            setSupportActionBar(toolbar)
    }

    protected fun showToast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(IconicsContextWrapper.wrap(CalligraphyContextWrapper.wrap(newBase)))
    }

}
