package `in`.co.webstersys.core.app

abstract class AppConstants {

    companion object {
       @JvmStatic
       var TIMEOUT_IN_SEC : Int = 30

        @JvmStatic
        var ENDPOINT : String = ""
    }

}