package `in`.co.webstersys.core.ui.networkstate




import `in`.co.webstersys.core.databinding.NetworkStateItemBinding
import `in`.co.webstersys.core.model.networkstate.NetworkState
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

/**
 * A View Holder that can display a loading or have click action.
 * It is used to show the network state of paging.
 */
class NetworkStateItemViewHolder(var binding: NetworkStateItemBinding, private val retryCallback: () -> Unit) : RecyclerView.ViewHolder(binding.getRoot()) {
    init {
        binding.retryButton.setOnClickListener {
            retryCallback()
        }
    }



    fun bindTo(networkState: NetworkState?) {
        binding.networkState = networkState!!
        binding.executePendingBindings()

    }

    companion object {
        fun create(inflater: LayoutInflater, parent: ViewGroup, retryCallback: () -> Unit): NetworkStateItemViewHolder {
            val networkStateItemBinding = NetworkStateItemBinding.inflate(inflater, parent, false)
            return NetworkStateItemViewHolder(networkStateItemBinding, retryCallback)
        }


    }
}