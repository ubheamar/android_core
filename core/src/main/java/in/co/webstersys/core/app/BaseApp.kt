package `in`.co.webstersys.core.app

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

open class BaseApp : Application() {

    init {
        app = this
    }
    companion object {
        var app : Application ? = null
        fun getInstance() : Application? {
            return app
        }
    }
}