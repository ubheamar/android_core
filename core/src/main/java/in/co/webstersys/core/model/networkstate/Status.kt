package `in`.co.webstersys.core.model.networkstate

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}