package `in`.co.webstersys.core.ui.base

import `in`.co.webstersys.core.R
import android.content.Context
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.AdapterView
import com.mikepenz.ionicons_typeface_library.Ionicons
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.BaseDrawerItem
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by ua989 on 2/22/2018.
 */
abstract class BaseNavActivity<DB : ViewDataBinding>() : BaseActivity<DB>() {
   /* @Inject
    lateinit var fragmentAndroidInjector: DispatchingAndroidInjector<Fragment>*/

    protected var drawer : Drawer? = null
    abstract fun onDrawerItemClick(view: View, identifier: Int, iDrawerItem: IDrawerItem<*, *>)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getNavigationDrawer(savedInstanceState)
    }

    /*override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return fragmentAndroidInjector
    }*/

    protected fun getNavigationDrawer(savedInstanceState: Bundle?) {
        if(toolbar!=null) {
            drawer = DrawerBuilder()
                    .withActivity(this)
                    .withSavedInstance(savedInstanceState)
                    .withToolbar(toolbar!!)
                    .withOnDrawerItemClickListener(MenuClickListner(this))
                    .build()
        }

    }


    private inner class MenuClickListner(internal var context: Context) : Drawer.OnDrawerItemClickListener {

        override fun onItemClick(view: View, identifier: Int, iDrawerItem: IDrawerItem<*, *>): Boolean {
            onDrawerItemClick(view,identifier,iDrawerItem)
            return true
        }
    }
}
