package `in`.co.webstersys.core.ui.base

import android.support.v7.widget.RecyclerView

/**
 * Created by UA989 on 3/16/2018.
 */

abstract class BaseAdapter<Type : RecyclerView.ViewHolder, Data> : RecyclerView.Adapter<Type>() {
    private var data: List<Data> = ArrayList<Data>()

    fun setData(data: List<Data>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    protected fun getItem(position: Int) : Data {
        return data.get(position)
    }
}