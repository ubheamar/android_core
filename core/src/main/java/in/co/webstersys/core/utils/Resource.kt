package `in`.co.webstersys.core.utils

import `in`.co.webstersys.core.model.networkstate.NetworkState
import android.arch.lifecycle.LiveData

/**
 * Created by ua989 on 3/14/2018.
 */
data class Resource<T>(
        // the LiveData of paged lists for the UI to observe
        val data: LiveData<T>,
        // represents the network request status to show to the user
        val networkState: LiveData<NetworkState>,
        // represents the refresh status to show to the user. Separate from networkState, this
        // value is importantly only when refresh is requested.
        /* val refreshState: LiveData<NetworkState>,*/
        // refreshes the whole data and fetches it from scratch.
        /*val refresh: () -> Unit,*/
        // retries any failed requests.
        val retry: () -> Unit)