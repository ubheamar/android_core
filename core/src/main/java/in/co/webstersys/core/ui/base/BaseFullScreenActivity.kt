package `in`.co.webstersys.core.ui.base

import `in`.co.webstersys.core.utils.ImmersiveModeListener
import android.content.Context
import android.databinding.ViewDataBinding
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.mikepenz.iconics.context.IconicsContextWrapper
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

/**
 * Created by Lord Shiva on 3/24/2018.
 */
abstract class BaseFullScreenActivity<DB : ViewDataBinding>() : BaseActivity<DB>() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //  supportActionBar?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(this, R.color.black_overlay)))
    }

    fun toggle(visibility: Int) {
        if (visibility == View.VISIBLE) {
            showSystemUI()
        } else {
            hideUI()
        }
    }

    private fun hideUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_IMMERSIVE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or //Full screen mode
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or     //Stable when using multiple flags
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or //avoid artifacts when FLAG_FULLSCREEN
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        } else {
            window.decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_FULLSCREEN or //Full screen mode
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or     //Stable when using multiple flags
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or //avoid artifacts when FLAG_FULLSCREEN
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
        immersiveModeListener?.onChangeImmersiveMode(false)
    }

    private fun showSystemUI() {
        //
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        immersiveModeListener?.onChangeImmersiveMode(true)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(IconicsContextWrapper.wrap(CalligraphyContextWrapper.wrap(newBase)))
    }

    protected abstract val immersiveModeListener: ImmersiveModeListener?


}