package `in`.co.webstersys.core.ui.base

import `in`.co.webstersys.core.R
import `in`.co.webstersys.core.model.networkstate.NetworkState
import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView

/**
 * Created by UA989 on 2/23/2018.
 */
abstract class BasePagedAdapter<T>(diffCallback: DiffUtil.ItemCallback<T>, private val retryCallback: () -> Unit)
    : PagedListAdapter<T, RecyclerView.ViewHolder>(diffCallback) {
    private var networkState: NetworkState? = null

    companion object {
        var TYPE_LOADING = R.layout.network_state_item
    }

    protected fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED
    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    protected fun getNetworkState(): NetworkState? {
        return networkState
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

}