package `in`.co.webstersys.core.utils

interface ImmersiveModeListener {
    fun onChangeImmersiveMode(show:Boolean)
}