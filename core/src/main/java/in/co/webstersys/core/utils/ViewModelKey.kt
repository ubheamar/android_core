package `in`.co.webstersys.core.utils

import dagger.MapKey
import kotlin.reflect.KClass
import android.arch.lifecycle.ViewModel
@MustBeDocumented
@Target(
        AnnotationTarget.FUNCTION,
        AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)